/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package papa;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author CSIE
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button button;
    private  Connection conn;
    @FXML
    private Label name;

  
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
         doconn();
         //int row = doInsert();
         int  row = doselect();
         label.setText("row "+row+" row");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
   private int doInsert(){
       int rows=0,x;
       String insert ="INSERT INTO TEST.COLLEAGUES (ID,FIRSTNAME,LASTNAME,TITLE,DEPARTMENT,EMAIL,LOGIN) VALUES(?,?,?,?,?,?,?)";
       PreparedStatement stmt =null;
        try {
            if(conn==null)
            {
                doconn();
            }
             stmt = conn.prepareStatement(insert);
            
            stmt.setInt(1,7);
            
            stmt.setString(2,"test");
             stmt.setString(3,"test");
             stmt.setString(4,"test");
             stmt.setString(5,"test");
             stmt.setString(6,"test");
             stmt.setInt(7,8);
             rows = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
           try {
               stmt.close();
               conn.close();
           } catch (SQLException ex) {
               Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
           }
            return rows;
        }
   }
    private void doconn() {
        try {
            conn = DriverManager.getConnection("jdbc:derby://localhost:1527/contact", "test", "test");
            if (conn != null) {
                label.setText("LINKED");
            }

        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            label.setText("Failuse");
        }
      

    }
     private int doselect(){
       int rows=0,x;
       String insert ="Select *From TEST.COLLEAGUES where ID = ?";
       PreparedStatement stmt =null;
       ResultSet rs=null;
        try {
            if(conn==null)
            {
                doconn();
            }
             stmt = conn.prepareStatement(insert);
            
            stmt.setInt(1,3);
            rs= stmt.executeQuery();
            while(rs.next()){
                name.setText(rs.getString("FIRSTNAME"));
                rows++;
            }
           
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
           try {
               stmt.close();
               conn.close();
           } catch (SQLException ex) {
               Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
           }
            return rows;
        }
   }
}

   